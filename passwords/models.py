from django.db import models

class Password(models.Model):
    website = models.CharField(max_length=150)
    email = models.EmailField(max_length=50)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    category = models.CharField(max_length=50)

def __str__(self):
    return self.website
