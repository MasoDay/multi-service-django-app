from django.contrib import admin
from passwords.models import Password

@admin.register(Password)
class Password(admin.ModelAdmin):
    list_display = [
        "website",
        "email",
        "username",
        "password",
        "category",
    ]
